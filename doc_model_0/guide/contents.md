*How-to goal is to show the steps to accomplish specific **tasks**.*

*They are aimed at intermediate and experienced users who might have completed the tutorials and are now trying to figure out how to adapt the tool to their needs.*

*A meta-goal is to answer the question of knowing if the tool is the appropriate tool for the task.*

-------------------------------------------------------------------------------------------------------

# How to...

This section includes step-by-step guides showing how to do specific tasks.

## How to connect to...

## How to interface with...

## How to save/record...
